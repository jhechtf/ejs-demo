const Express = require('express');
const app = Express();
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
  res.render('templates/main');
});

app.get('/new', (req, res) => {
  res.render('templates/main', { pageTitle: 'Hello'})
})

app.listen(3000);